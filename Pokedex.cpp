//
// Pokemon.cpp for Pokemon in /home/bengle_b/test/rush_pokemon
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Fri Jan 23 19:15:39 2015 Bengler Bastien
// Last update Sat Jan 24 20:04:29 2015 Thomas Chartier
//

#include "Pokedex.hpp"
#include "Pokemon.hpp"
#include <iostream>
#include <fstream>
#include <algorithm>

Pokedex::Pokedex()
{
}

Pokedex::~Pokedex()
{
}

Pokedex::Pokedex(const Pokedex& pokedex)
{
      _listPokemon = pokedex._listPokemon;
}

Pokedex&	Pokedex::operator=(const Pokedex& pokedex)
{
  if (&pokedex != this)
    {
      _listPokemon = pokedex._listPokemon;
    }
  return *this;
}

static void displayforeach(Pokemon pokemon)
{
  std::cout << "id : " << pokemon.getId() <<
    std::endl << "name : " << pokemon.getName() << std::endl
    //	    << "type : " << pokemon.getType() << std::endl
	    << "species : " << pokemon.getSpecies() << std::endl
  	    << "desc : " << pokemon.getDesc() << std::endl
   	    << "---------------------------------------" << std::endl;
}

void	Pokedex::displayPokemon()
{
  std::for_each(_listPokemon.begin(), _listPokemon.end(), displayforeach);
}

void	Pokedex::getElementFromXML(const std::string& line, Pokemon& pokemon)
{
  if (line.find("pokemon id", 0) < line.length())
    parseIdPokemon(line, pokemon);
  if (pokemon.getId().length() && line.find("name", 0) < line.length())
    parseNamePokemon(line, pokemon);
  //  if (line.find("type", 0) < line.length())
  // parseTypePokemon(line, pokemon);
  if (line.find("species", 0) < line.length())
    parseSpeciesPokemon(line, pokemon);
  if (line.find("description", 0) < line.length())
    parseDescriptionPokemon(line, pokemon);
  if (pokemon.getId().length() > 0 && 
      (pokemon.getName()).length() > 0 &&
      (pokemon.getSpecies()).length() > 0 &&
      (pokemon.getDesc()).length() > 0)
    {
      _listPokemon.push_back(pokemon);
      pokemon.resetPokemon(pokemon);
    }
}

void	Pokedex::parserXML()
{
  std::string line;
  std::ifstream myfile("pokedata.xml");
  Pokemon pokemon;

  if (myfile.is_open())
    {
      while (getline(myfile, line))
	getElementFromXML(line, pokemon);
      myfile.close();	    
    }
}

void	Pokedex::parseNamePokemon(const std::string &line, Pokemon &pokemon)
{
  int	min = line.find(">", 0);
  int	max = line.find("<", min + 1);
  std::string name;

  while (min < max - 1)
    name += line.at(++min);
  if (pokemon.getName().length() == 0)
    pokemon.setName(name);
}

void	Pokedex::parseIdPokemon(const std::string &line, Pokemon &pokemon)
{
  std::string id;

  int	min = line.find("\"", 0);
  int	max = line.find("\"", min + 1);
  while (min < max - 1)
    id += line.at(++min);
  pokemon.setId(id);
}

void	Pokedex::parseTypePokemon(const std::string &line, Pokemon &pokemon)
{
  std::string type;

  int	min = line.find(">", 0);
  int	max = line.find("<", min + 1);
  while (min < max - 1)
    type += line.at(++min);
  if (type.length() > 0)
    pokemon.getType().push_back(type);
}

void	Pokedex::parseSpeciesPokemon(const std::string &line, Pokemon &pokemon)
{
  std::string species;

  int	min = line.find(">", 0);
  int	max = line.find("<", min + 1);
  while (min < max - 1)
    species += line.at(++min);
  pokemon.setSpecies(species);
}

void	Pokedex::parseDescriptionPokemon(const std::string &line, Pokemon &pokemon)
{
  std::string desc;

  int	min = line.find(">", 0);
  int	max = line.find("<", min + 1);
  while (min < max - 1)
    desc += line.at(++min);
  pokemon.setDesc(desc);
}

std::list<Pokemon>::iterator	Pokedex::getListPokemon()
{
  return _listPokemon.begin();
}
