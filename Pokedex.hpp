//
// Pokemon.hpp for Pokemon in /home/bengle_b/test/rush_pokemon
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Wed Jan 21 19:13:07 2015 Bengler Bastien
// Last update Sat Jan 24 18:54:12 2015 Thomas Chartier
//

#ifndef POKEDEX_HPP_
# define POKEDEX_HPP_

#include "Pokemon.hpp"
#include <list>
#include <iostream>

class	Pokedex
{
public:
  Pokedex();
  ~Pokedex();
  Pokedex(const Pokedex&);
  Pokedex& operator=(const Pokedex&);

  void parserXML();
  void getElementFromXML(const std::string& line, Pokemon& pokemon);
  void parseNamePokemon(const std::string& line, Pokemon &pokemon);
  void parseIdPokemon(const std::string& line, Pokemon &pokemon);
  void parseDescriptionPokemon(const std::string& line, Pokemon &pokemon);
  void parseSpeciesPokemon(const std::string& line, Pokemon &pokemon);
  void parseTypePokemon(const std::string& line, Pokemon &pokemon);

  void displayPokemon();
  void resetPokemon(Pokedex& pokemon);
  std::list<Pokemon>::iterator getListPokemon();
  std::list<Pokemon> _listPokemon;
};

#endif /* POKEDEX_HPP_ */
