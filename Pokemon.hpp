//
// Pokemon.hpp for Pokemon in /home/bengle_b/test/rush_pokemon
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Sat Jan 24 13:07:17 2015 Bengler Bastien
// Last update Sat Jan 24 18:35:56 2015 Bengler Bastien
//

#ifndef POKEMON_HPP_
# define POKEMON_HPP_

#include <iostream>
#include <vector>

class	Pokemon
{
public:

  Pokemon();
  ~Pokemon();
  Pokemon(const Pokemon&);
  Pokemon& operator=(const Pokemon&);

  void setId(const std::string& id);
  const std::string& getId() const;

  void setName(const std::string& name);
  const std::string& getName() const;

  std::vector<std::string>& getType();

  void setDesc(const std::string& desc);
  const std::string& getDesc() const;

  void setSpecies(const std::string& species);
  const std::string& getSpecies() const;

  void resetPokemon(Pokemon& pokemon);

private:
  std::string _id;
  std::string _name;
  std::string _desc;
  std::string _species;
  std::vector<std::string> _type;
};

#endif /* POKEMON_HPP */
