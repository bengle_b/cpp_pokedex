//
// Pokemon.cpp for Pokemon in /home/bengle_b/test/rush_pokemon
// 
// Made by Bengler Bastien
// Login   <bengle_b@epitech.net>
// 
// Started on  Sat Jan 24 13:04:57 2015 Bengler Bastien
// Last update Sat Jan 24 18:35:52 2015 Bengler Bastien
//

#include "Pokemon.hpp"
#include <iostream>
#include <list>

Pokemon::Pokemon()
{
  _id = "";
  _name = "";
  _desc = "";
  _species = "";
}

Pokemon::~Pokemon()
{
}

Pokemon::Pokemon(const Pokemon& pokemon)
{
  _id = pokemon.getId();
  _name = pokemon.getName();
  _desc = pokemon.getDesc();
  _species = pokemon.getSpecies();
}

Pokemon&	Pokemon::operator=(const Pokemon& pokemon)
{
  if (&pokemon != this)
    {
      _id = pokemon.getId();
      _name = pokemon.getName();
      _desc = pokemon.getDesc();
      _species = pokemon.getSpecies();
    }
  return *this;
}

void	Pokemon::setId(const std::string& id)
{
  _id = id;
}

const std::string&	Pokemon::getId() const
{
  return _id;
}

void	Pokemon::setName(const std::string& name)
{
  _name = name;
}

const std::string&	Pokemon::getName() const
{
  return _name;
}

std::vector<std::string>&	Pokemon::getType()
{
  return _type;
}

void	Pokemon::setDesc(const std::string& desc)
{
  _desc = desc;
}

const std::string&	Pokemon::getDesc() const
{
  return _desc;
}

void	Pokemon::setSpecies(const std::string& species)
{
  _species = species;
}

const std::string&	Pokemon::getSpecies() const
{
  return _species;
}

void	Pokemon::resetPokemon(Pokemon& pokemon)
{
  pokemon.setId("");
  pokemon.setName("");
  pokemon.setDesc("");
  pokemon.setSpecies("");
}
