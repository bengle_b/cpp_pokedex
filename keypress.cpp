//
// keypress.cpp for key in /home/charti_t/test/pokedex
// 
// Made by Thomas Chartier
// Login   <charti_t@epitech.net>
// 
// Started on  Wed Jan 21 19:16:32 2015 Thomas Chartier
// Last update Sat Jan 24 20:05:03 2015 Thomas Chartier
//

#include <QApplication>
#include <QKeyEvent>
#include "keypress.h"
#include <iostream>
#include <QFont>
#include <QWidget>
#include <QLabel>
#include <list>
#include <QString>
#include <iterator>
#include "Pokedex.hpp"



KeyPress::KeyPress(QWidget *parent)
  : QWidget(parent)
{
  i = 1;
  setStyleSheet("QWidget{ background-image : url(img/pokedex_fond.jpg); background-repeat : no-repeat;}");
  setWindowFlags( Qt::CustomizeWindowHint );
  resize(533,800);

  pokemon.parserXML();

  pokename = new QLabel(this);  
  label = new QLabel(this);
  pokespec = new QLabel(this);
  pokedesc = new QLabel(this);
  pokestats = new QLabel(this);


  QString name = QString::fromStdString(pokemon._listPokemon.front().getName());
  pokename->setFont(QFont("Courrier",15));
  pokename->setAttribute(Qt::WA_TranslucentBackground);
  pokename->resize(150,20);
  pokename->setText(name);
  pokename->move(50,608);

  QString spec = QString::fromStdString(pokemon._listPokemon.front().getSpecies());
  pokespec->setFont(QFont("Courrier",15));
  pokespec->setAttribute(Qt::WA_TranslucentBackground);
  pokespec->resize(400,20);
  pokespec->setText(spec);
  pokespec->move(290,608);

  QString desc = QString::fromStdString(pokemon._listPokemon.front().getDesc());
  pokedesc->setWordWrap(true);
  pokedesc->setAttribute(Qt::WA_TranslucentBackground);
  pokedesc->move(170,647);
  pokedesc->resize(210,100);
  pokedesc->setFont(QFont("Courrier",11));
  pokedesc->setText(desc);

  label->move(120,200);
  std::string tmp = "./img/" + pokemon._listPokemon.front().getId() + ".jpg";
  QString path = QString::fromStdString(tmp);
  label->setPixmap(QPixmap(path).scaled(300,280));
}

void KeyPress::keyPressEvent(QKeyEvent *event)
{
  std::list<Pokemon>::iterator poke = pokemon.getListPokemon();
  
  if (event->key() == Qt::Key_Escape)   
    qApp->quit(); 
  
  if (event->key() == Qt::Key_Left)
    {
      i--;
      if (i < 1)
	i = 493;
      std::advance(poke,i-1);

      label->close();
      label->move(120,200);
      std::string tmp = "./img/" + poke->getId() + ".jpg";
      QString path = QString::fromStdString(tmp);
      label->setPixmap(QPixmap(path).scaled(290,280));
      label->show();

      QString name = QString::fromStdString(poke->getName());
      pokename->setAttribute(Qt::WA_TranslucentBackground);
      pokename->setText(name);
      pokename->move(50,608);

      QString spec = QString::fromStdString(poke->getSpecies());
      pokespec->resize(400,20);
      pokespec->setText(spec);
      pokespec->move(300,608);

      QString desc = QString::fromStdString(poke->getDesc());
      pokedesc->move(170,647);
      pokedesc->resize(210,100);
      pokedesc->setText(desc);
    }

  if (event->key() == Qt::Key_Right)
    {

      i++;
      if (i>493)
	i = 1;
      std::advance(poke,i-1);
      label->close();
      label->move(120,200);
      std::string tmp = "./img/" + poke->getId() + ".jpg";
      QString path = QString::fromStdString(tmp);
      label->setPixmap(QPixmap(path).scaled(300,280));
      label->show();

      QString name = QString::fromStdString(poke->getName());
      pokename->setAttribute(Qt::WA_TranslucentBackground);
      pokename->setText(name);
      pokename->move(50,608);     

      QString spec = QString::fromStdString(poke->getSpecies());
      pokespec->resize(400,20);
      pokespec->setText(spec);
      pokespec->move(290,608);

      QString desc = QString::fromStdString(poke->getDesc());
      pokedesc->move(170,647);
      pokedesc->resize(210,100);
      pokedesc->setText(desc);
    }
  /*  if(event->key() == Qt::Key_Return)
    {
      label->close();
      QString stat = QString::fromStdString( "ATK = 15 DEF = 69");
      pokestats->move(120,200);
      pokestats->setFont(QFont("Courrier",10));
      pokestats->setAttribute(Qt::WA_TranslucentBackground);
      pokestats->resize(300,300);
      pokestats->setText(stat);
      pokestats->show();
    }
  if(event->key() == Qt::Key_Backspace)
    {
      pokestats->close();
      label->show();
    }*/
}
