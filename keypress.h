/*
** keypress.h for key in /home/charti_t/test/pokedex
** 
** Made by Thomas Chartier
** Login   <charti_t@epitech.net>
** 
** Started on  Wed Jan 21 19:16:06 2015 Thomas Chartier
** Last update Sat Jan 24 19:36:07 2015 Thomas Chartier
*/

#ifndef KEYPRESS_H_
# define KEYPRESS_H_

#include <QWidget>
#include <QLabel>
#include "Pokedex.hpp"

class KeyPress : public QWidget
{
 public:
  KeyPress(QWidget *parent = 0);
  QLabel *label;
  QLabel *pokename;
  QLabel *pokespec;
  QLabel *pokestats;
  QLabel *pokedesc;
  Pokedex pokemon;
  int	i;
 protected:
  void keyPressEvent(QKeyEvent * e);
};


#endif /* !KEYPRESS_H_ */
